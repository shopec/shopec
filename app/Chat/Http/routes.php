<?php

Route::group(['prefix' => 'chat', 'namespace' => 'Chat\Http\Controllers'], function()
{
	Route::get('/', 'ChatController@index');
});
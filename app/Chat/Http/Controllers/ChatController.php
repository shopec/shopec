<?php namespace Chat\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class ChatController extends Controller {
	
	public function index()
	{
		return view('chat::index');
	}
	
}
<?php

Route::group(['prefix' => 'circle', 'namespace' => 'Circle\Http\Controllers'], function()
{
	Route::get('/', 'CircleController@index');
});
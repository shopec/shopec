<?php namespace Circle\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class CircleController extends Controller {
	
	public function index()
	{
		return view('circle::index');
	}
	
}
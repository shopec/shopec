<?php

Route::group(['prefix' => 'wap', 'namespace' => 'Wap\Http\Controllers'], function()
{
	Route::get('/', 'WapController@index');
});
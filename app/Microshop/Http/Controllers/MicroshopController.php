<?php namespace Microshop\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class MicroshopController extends Controller {
	
	public function index()
	{
		return view('microshop::index');
	}
	
}
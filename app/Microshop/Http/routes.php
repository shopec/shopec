<?php

Route::group(['prefix' => 'microshop', 'namespace' => 'Microshop\Http\Controllers'], function()
{
	Route::get('/', 'MicroshopController@index');
});
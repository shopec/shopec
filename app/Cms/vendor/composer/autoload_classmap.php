<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Cms\\Database\\Seeders\\CmsDatabaseSeeder' => $baseDir . '/Database/Seeders/CmsDatabaseSeeder.php',
    'Cms\\Http\\Controllers\\CmsController' => $baseDir . '/Http/Controllers/CmsController.php',
    'Cms\\Providers\\CmsServiceProvider' => $baseDir . '/Providers/CmsServiceProvider.php',
);

<?php

Route::group(['prefix' => 'cms', 'namespace' => 'Cms\Http\Controllers'], function()
{
	Route::get('/', 'CmsController@index');
});
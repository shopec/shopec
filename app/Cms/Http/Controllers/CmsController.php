<?php namespace Cms\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class CmsController extends Controller {
	
	public function index()
	{
		return view('cms::index');
	}
	
}
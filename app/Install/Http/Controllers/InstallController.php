<?php namespace Install\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class InstallController extends Controller {
	
	public function index()
	{
		return view('install::index');
	}
	
}
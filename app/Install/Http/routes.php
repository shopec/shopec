<?php

Route::group(['prefix' => 'install', 'namespace' => 'Install\Http\Controllers'], function()
{
	Route::get('/', 'InstallController@index');
});
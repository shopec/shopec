<?php

Route::group(['prefix' => 'shop', 'namespace' => 'Shop\Http\Controllers'], function()
{
	Route::get('/', 'ShopController@index');
});
<?php namespace Shop\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class ShopController extends Controller {
	
	public function index()
	{
		return view('shop::index');
	}
	
}
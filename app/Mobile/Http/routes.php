<?php

Route::group(['prefix' => 'mobile', 'namespace' => 'Mobile\Http\Controllers'], function()
{
	Route::get('/', 'MobileController@index');
});
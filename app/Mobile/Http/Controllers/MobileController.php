<?php namespace Mobile\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class MobileController extends Controller {
	
	public function index()
	{
		return view('mobile::index');
	}
	
}
<?php

Route::group(['prefix' => 'api', 'namespace' => 'Api\Http\Controllers'], function()
{
	Route::get('/', 'ApiController@index');
});
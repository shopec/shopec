<?php

Route::group(['prefix' => 'framework', 'namespace' => 'Framework\Http\Controllers'], function()
{
	Route::get('/', 'FrameworkController@index');
});
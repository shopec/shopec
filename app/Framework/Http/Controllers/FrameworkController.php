<?php namespace Framework\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class FrameworkController extends Controller {
	
	public function index()
	{
		return view('framework::index');
	}
	
}
<?php

Route::group(['prefix' => 'delivery', 'namespace' => 'Delivery\Http\Controllers'], function()
{
	Route::get('/', 'DeliveryController@index');
});
<?php namespace Delivery\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class DeliveryController extends Controller {
	
	public function index()
	{
		return view('delivery::index');
	}
	
}
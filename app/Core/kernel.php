<?php

use Illuminate\Foundation\Application;

defined('DS') or define('DS',DIRECTORY_SEPARATOR);

/**
 * Created by PhpStorm.
 * User: mo
 * Date: 16-2-1
 * Time: 下午4:53
 */
class kernel extends Application
{

    public static function start(){
        /**
         * Laravel - A PHP Framework For Web Artisans
         *
         * @package  Laravel
         * @author   Taylor Otwell <taylorotwell@gmail.com>
         */

        /*
        |--------------------------------------------------------------------------
        | Register The Auto Loader
        |--------------------------------------------------------------------------
        |
        | Composer provides a convenient, automatically generated class loader for
        | our application. We just need to utilize it! We'll simply require it
        | into the script here so that we don't have to worry about manual
        | loading any of our classes later on. It feels nice to relax.
        |
        */

        require __DIR__.'/bootstrap/autoload.php';

        /*
        |--------------------------------------------------------------------------
        | Turn On The Lights
        |--------------------------------------------------------------------------
        |
        | We need to illuminate PHP development, so let us turn on the lights.
        | This bootstraps the framework and gets it ready for use, then it
        | will load up this application so that we can run it and send
        | the responses back to the browser and delight our users.
        |
        */

        $app = require_once __DIR__.'/bootstrap/app.php';

        /*
        |--------------------------------------------------------------------------
        | Run The Application
        |--------------------------------------------------------------------------
        |
        | Once we have the application, we can simply call the run method,
        | which will execute the request and send the response back to
        | the client's browser allowing them to enjoy the creative
        | and wonderful application we have prepared for them.
        |
        */

        $kernel = $app->make('Illuminate\Contracts\Http\Kernel');

        $response = $kernel->handle(
            $request = Illuminate\Http\Request::capture()
        );

        $response->send();

        $kernel->terminate($request, $response);

    }
    /**
     * Get the path to the application "app" directory.
     *
     * @return string
     */
    public function path()
    {

        $path = parent::path();

        return $this->basePath.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'Base';
    }

    /**
     * Get the path to the database directory.
     *
     * @return string
     */
    public function databasePath()
    {
        return $this->databasePath ?: $this->basePath.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'Database';
    }

    /**
     * Get the path to the public / web directory.
     *
     * @return string
     */
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'public';
    }

    /**
     * Get the path to the storage directory.
     *
     * @return string
     */
    public function storagePath()
    {
        return $this->storagePath ?: $this->basePath.DIRECTORY_SEPARATOR.'sysdata';
    }

    /**
     * Get the path to the language files.
     *
     * @return string
     */
    public function langPath()
    {
        return join(DS,array($this->basePath,'Base', 'Resources','lang'));
    }
}
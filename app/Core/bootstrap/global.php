<?php
/**
 * 入口文件
 *
 * 统一入口，进行初始化信息
 *
 ***/

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

define('BASE_ROOT_PATH', str_replace('\\', '/', dirname(__FILE__)));
/**
 * 安装判断
 */
if (!is_file(BASE_ROOT_PATH . "/install/lock") && is_file(BASE_ROOT_PATH . "/install/index.php")) {
    if (ProjectName != 'shop') {
        @header("location: ../install/index.php");
    } else {
        @header("location: install/index.php");
    }
    exit;
}
define('BASE_CORE_PATH', BASE_ROOT_PATH . '/core');
define('BASE_DATA_PATH', BASE_ROOT_PATH . '/data');
define('DS', '/');
define('InShopNC', true);
define('StartTime', microtime(true));
define('TIMESTAMP', time());
define('DIR_SHOP', 'shop');
define('DIR_CMS', 'cms');
define('DIR_CIRCLE', 'circle');
define('DIR_MICROSHOP', 'microshop');
define('DIR_ADMIN', 'admin');
define('DIR_API', 'api');
define('DIR_MOBILE', 'mobile');
define('DIR_WAP', 'wap');

define('DIR_RESOURCE', 'data/resource');
define('DIR_UPLOAD', 'data/upload');

define('ATTACH_PATH', 'shop');
define('ATTACH_COMMON', ATTACH_PATH . '/common');
define('ATTACH_AVATAR', ATTACH_PATH . '/avatar');
define('ATTACH_EDITOR', ATTACH_PATH . '/editor');
define('ATTACH_MEMBERTAG', ATTACH_PATH . '/membertag');
define('ATTACH_STORE', ATTACH_PATH . '/store');
define('ATTACH_GOODS', ATTACH_PATH . '/store/goods');
define('ATTACH_STORE_DECORATION', ATTACH_PATH . '/store/decoration');
define('ATTACH_LOGIN', ATTACH_PATH . '/login');
define('ATTACH_WAYBILL', ATTACH_PATH . '/waybill');
define('ATTACH_ARTICLE', ATTACH_PATH . '/article');
define('ATTACH_BRAND', ATTACH_PATH . '/brand');
define('ATTACH_ADV', ATTACH_PATH . '/adv');
define('ATTACH_ACTIVITY', ATTACH_PATH . '/activity');
define('ATTACH_WATERMARK', ATTACH_PATH . '/watermark');
define('ATTACH_POINTPROD', ATTACH_PATH . '/pointprod');
define('ATTACH_GROUPBUY', ATTACH_PATH . '/groupbuy');
define('ATTACH_LIVE_GROUPBUY', ATTACH_PATH . '/livegroupbuy');
define('ATTACH_SLIDE', ATTACH_PATH . '/store/slide');
define('ATTACH_VOUCHER', ATTACH_PATH . '/voucher');
define('ATTACH_STORE_JOININ', ATTACH_PATH . '/store_joinin');
define('ATTACH_REC_POSITION', ATTACH_PATH . '/rec_position');
define('ATTACH_MOBILE', 'mobile');
define('ATTACH_CIRCLE', 'circle');
define('ATTACH_CMS', 'cms');
define('ATTACH_LIVE', 'live');
define('ATTACH_MALBUM', ATTACH_PATH . '/member');
define('ATTACH_MICROSHOP', 'microshop');
define('TPL_SHOP_NAME', 'default');
define('TPL_CIRCLE_NAME', 'default');
define('TPL_MICROSHOP_NAME', 'default');
define('TPL_CMS_NAME', 'default');
define('TPL_ADMIN_NAME', 'default');

/*
 * 商家入驻状态定义
 */
//新申请
define('STORE_JOIN_STATE_NEW', 10);
//完成付款
define('STORE_JOIN_STATE_PAY', 11);
//初审成功
define('STORE_JOIN_STATE_VERIFY_SUCCESS', 20);
//初审失败
define('STORE_JOIN_STATE_VERIFY_FAIL', 30);
//付款审核失败
define('STORE_JOIN_STATE_PAY_FAIL', 31);
//开店成功
define('STORE_JOIN_STATE_FINAL', 40);

//默认颜色规格id(前台显示图片的规格)
define('DEFAULT_SPEC_COLOR_ID', 1);


/**
 * 商品图片
 */
define('GOODS_IMAGES_WIDTH', '60,240,360,1280');
define('GOODS_IMAGES_HEIGHT', '60,240,360,12800');
define('GOODS_IMAGES_EXT', '_60,_240,_360,_1280');

/**
 *  订单状态
 */
//已取消
define('ORDER_STATE_CANCEL', 0);
//已产生但未支付
define('ORDER_STATE_NEW', 10);
//已支付
define('ORDER_STATE_PAY', 20);
//已发货
define('ORDER_STATE_SEND', 30);
//已收货，交易成功
define('ORDER_STATE_SUCCESS', 40);
//未付款订单，自动取消的天数
define('ORDER_AUTO_CANCEL_DAY', 3);
//已发货订单，自动确认收货的天数
define('ORDER_AUTO_RECEIVE_DAY', 7);
//兑换码支持过期退款，可退款的期限，默认为7天
define('CODE_INVALID_REFUND', 7);
//默认未删除
define('ORDER_DEL_STATE_DEFAULT', 0);
//已删除
define('ORDER_DEL_STATE_DELETE', 1);
//彻底删除
define('ORDER_DEL_STATE_DROP', 2);
//订单结束后可评论时间，15天，60*60*24*15
define('ORDER_EVALUATE_TIME', 1296000);
//抢购订单状态
define('OFFLINE_ORDER_CANCEL_TIME', 3);//单位为天


// extra

if (!@include(BASE_DATA_PATH . '/config/config.ini.php')) exit('config.ini.php isn\'t exists!');
if (file_exists(BASE_PATH . '/config/config.ini.php')) {
    include(BASE_PATH . '/config/config.ini.php');
}
global $config;

//默认平台店铺id
define('DEFAULT_PLATFORM_STORE_ID', $config['default_store_id']);

define('URL_MODEL', $config['url_model']);
$auto_site_url = strtolower('http://' . $_SERVER['HTTP_HOST'] . implode('/', $tmp_array));
define(SUBDOMAIN_SUFFIX, $config['subdomain_suffix']);
define('BASE_SITE_URL', $config['base_site_url']);
define('SHOP_SITE_URL', $config['shop_site_url']);
define('CMS_SITE_URL', $config['cms_site_url']);
define('CIRCLE_SITE_URL', $config['circle_site_url']);
define('MICROSHOP_SITE_URL', $config['microshop_site_url']);
define('ADMIN_SITE_URL', $config['admin_site_url']);
define('MOBILE_SITE_URL', $config['mobile_site_url']);
define('WAP_SITE_URL', $config['wap_site_url']);
define('UPLOAD_SITE_URL', $config['upload_site_url']);
define('RESOURCE_SITE_URL', $config['resource_site_url']);
define('DELIVERY_SITE_URL', $config['delivery_site_url']);

define('BASE_DATA_PATH', BASE_ROOT_PATH . '/data');
define('BASE_UPLOAD_PATH', BASE_DATA_PATH . '/upload');
define('BASE_RESOURCE_PATH', BASE_DATA_PATH . '/resource');

define('CHARSET', $config['db'][1]['dbcharset']);
define('DBDRIVER', $config['dbdriver']);
define('SESSION_EXPIRE', $config['session_expire']);
define('LANG_TYPE', $config['lang_type']);
define('COOKIE_PRE', $config['cookie_pre']);

define('DBPRE', $config['tablepre']);
define('DBNAME', $config['db'][1]['dbname']);
$_GET['act'] = $_GET['act'] ? strtolower($_GET['act']) : ($_POST['act'] ? strtolower($_POST['act']) : null);
$_GET['op'] = $_GET['op'] ? strtolower($_GET['op']) : ($_POST['op'] ? strtolower($_POST['op']) : null);

if (empty($_GET['act'])) {
    require_once(BASE_CORE_PATH . '/framework/core/route.php');
    new Route($config);
}
//统一ACTION
$_GET['act'] = preg_match('/^[\w]+$/i', $_GET['act']) ? $_GET['act'] : 'index';
$_GET['op'] = preg_match('/^[\w]+$/i', $_GET['op']) ? $_GET['op'] : 'index';

//对GET POST接收内容进行过滤,$ignore内的下标不被过滤
$ignore = array('article_content', 'pgoods_body', 'doc_content', 'content', 'sn_content', 'g_body', 'store_description', 'p_content', 'groupbuy_intro', 'remind_content', 'note_content', 'ref_url', 'adv_pic_url', 'adv_word_url', 'adv_slide_url', 'appcode', 'mail_content');
if (!class_exists('Security')) require(BASE_CORE_PATH . '/framework/libraries/security.php');
$_GET = !empty($_GET) ? Security::getAddslashesForInput($_GET, $ignore) : array();
$_POST = !empty($_POST) ? Security::getAddslashesForInput($_POST, $ignore) : array();
$_REQUEST = !empty($_REQUEST) ? Security::getAddslashesForInput($_REQUEST, $ignore) : array();
$_SERVER = !empty($_SERVER) ? Security::getAddSlashes($_SERVER) : array();

//启用ZIP压缩
if ($config['gzip'] == 1 && function_exists('ob_gzhandler') && $_GET['inajax'] != 1) {
    ob_start('ob_gzhandler');
} else {
    ob_start();
}
require_once(BASE_CORE_PATH . '/framework/libraries/queue.php');
require_once(BASE_CORE_PATH . '/framework/function/core.php');
require_once(BASE_CORE_PATH . '/framework/core/base.php');

require_once(BASE_CORE_PATH . '/framework/function/goods.php');

if (function_exists('spl_autoload_register')) {
    spl_autoload_register(array('Base', 'autoload'));
} else {
    function __autoload($class)
    {
        return Base::autoload($class);
    }
}

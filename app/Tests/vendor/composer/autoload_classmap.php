<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Tests\\Database\\Seeders\\TestsDatabaseSeeder' => $baseDir . '/Database/Seeders/TestsDatabaseSeeder.php',
    'Tests\\Http\\Controllers\\TestsController' => $baseDir . '/Http/Controllers/TestsController.php',
    'Tests\\Providers\\TestsServiceProvider' => $baseDir . '/Providers/TestsServiceProvider.php',
);

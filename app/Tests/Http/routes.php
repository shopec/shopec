<?php

Route::group(['prefix' => 'tests', 'namespace' => 'Tests\Http\Controllers'], function()
{
	Route::get('/', 'TestsController@index');
});
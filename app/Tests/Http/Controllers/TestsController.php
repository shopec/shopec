<?php namespace Tests\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class TestsController extends Controller {
	
	public function index()
	{
		return view('tests::index');
	}
	
}
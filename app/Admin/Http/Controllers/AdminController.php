<?php namespace Admin\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class AdminController extends Controller {
	
	public function index()
	{
		return view('admin::index');
	}
	
}